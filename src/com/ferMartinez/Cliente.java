package com.ferMartinez;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "clientes", schema = "basehibernate", catalog = "")
public class Cliente {
    private List<Menu> menus;

    @ManyToMany
        @JoinTable(name = "cliente_menu", catalog = "", schema = "basehibernate", joinColumns = @JoinColumn(name = "id_menu", referencedColumnName = "id_cliente", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_cliente", referencedColumnName = "id_menu", nullable = false))
    public List<Menu> getMenus() {
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }
}
