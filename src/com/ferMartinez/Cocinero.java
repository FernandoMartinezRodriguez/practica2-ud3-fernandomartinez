package com.ferMartinez;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "cocineros", schema = "basehibernate", catalog = "")
public class Cocinero {
    private List<Plato> platos;

    @OneToMany(mappedBy = "cocineros")
    public List<Plato> getPlatos() {
        return platos;
    }

    public void setPlatos(List<Plato> platos) {
        this.platos = platos;
    }
}
