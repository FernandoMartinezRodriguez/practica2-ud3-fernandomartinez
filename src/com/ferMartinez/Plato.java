package com.ferMartinez;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "platos", schema = "basehibernate", catalog = "")
public class Plato {
    private Cocinero cocineros;
    private List<Menu> menus;

    @ManyToOne
    @JoinColumn(name = "id_cocinero", referencedColumnName = "id_cocinero", nullable = false)
    public Cocinero getCocineros() {
        return cocineros;
    }

    public void setCocineros(Cocinero cocineros) {
        this.cocineros = cocineros;
    }

    @ManyToMany
    @JoinTable(name = "cliente_menu", catalog = "", schema = "basehibernate", joinColumns = @JoinColumn(name = "id_menu", referencedColumnName = "id_plato", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_menu", referencedColumnName = "id_menu", nullable = false))
    public List<Menu> getMenus() {
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }
}
