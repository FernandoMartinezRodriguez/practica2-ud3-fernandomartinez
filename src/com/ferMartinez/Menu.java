package com.ferMartinez;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "menus", schema = "basehibernate", catalog = "")
public class Menu {
    private List<Cliente> clientes;
    private List<Plato> platos;

    @ManyToMany(mappedBy = "menus")
    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    @ManyToMany(mappedBy = "menus")
    public List<Plato> getPlatos() {
        return platos;
    }

    public void setPlatos(List<Plato> platos) {
        this.platos = platos;
    }
}
