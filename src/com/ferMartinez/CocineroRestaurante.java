package com.ferMartinez;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cocinero_restaurante", schema = "basehibernate", catalog = "")
public class CocineroRestaurante {
    private Restaurante restaurantes;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id_restaurante", nullable = false)
    public Restaurante getRestaurantes() {
        return restaurantes;
    }

    public void setRestaurantes(Restaurante restaurantes) {
        this.restaurantes = restaurantes;
    }
}
