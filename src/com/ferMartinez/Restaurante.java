package com.ferMartinez;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "restaurantes", schema = "basehibernate", catalog = "")
public class Restaurante {
    private List<CocineroRestaurante> trabajos;

    @OneToMany(mappedBy = "restaurantes")
    public List<CocineroRestaurante> getTrabajos() {
        return trabajos;
    }

    public void setTrabajos(List<CocineroRestaurante> trabajos) {
        this.trabajos = trabajos;
    }
}
